export interface IRestaurantChainLocation {
  chainLocationId: number;
  chainName: string;
  cityStateName: string;
}
