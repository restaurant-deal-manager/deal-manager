export type { ICustomer } from "./ICustomer";
export type { ILocationDeal } from "./ILocationDeal";
export type { IDeal } from "./IDeal";
export type { IRestaurantChain } from "./IRestaurantChain";
export type { ICustomerRestaurantTransaction } from "./ICustomerRestaurantTransaction";
export type { IRestaurantChainLocation } from "./IRestaurantChainLocation";
