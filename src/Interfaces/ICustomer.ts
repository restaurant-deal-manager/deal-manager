export interface ICustomer {
  discountCardNumber: number;
  lastName: string;
  firstName: string;
  birthDate: string;
}
