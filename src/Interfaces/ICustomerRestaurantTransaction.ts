export interface ICustomerRestaurantTransaction {
  transactionId: number;
  discountCardNumber: number;
  chainLocationId: number;
  dealId: number;
  date: string;
}
