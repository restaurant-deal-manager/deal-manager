export interface IDeal {
  dealId: number;
  percentDiscount: number;
}
