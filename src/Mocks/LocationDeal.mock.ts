import { ILocationDeal } from "../Interfaces";

export const LocationDealsMock: ILocationDeal[] = [
  {
    dealId: 1,
    chainLocationId: 50,
  },
  {
    dealId: 2,
    chainLocationId: 25,
  },
  {
    dealId: 3,
    chainLocationId: 33,
  },
  {
    dealId: 4,
    chainLocationId: 40,
  },
  {
    dealId: 5,
    chainLocationId: 45,
  },
  {
    dealId: 6,
    chainLocationId: 90,
  },
  {
    dealId: 7,
    chainLocationId: 10,
  },
  {
    dealId: 8,
    chainLocationId: 15,
  },
  {
    dealId: 9,
    chainLocationId: 12,
  },
  {
    dealId: 10,
    chainLocationId: 13,
  },
];

export default LocationDealsMock;
