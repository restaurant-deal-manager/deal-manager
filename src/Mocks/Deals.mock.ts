import { IDeal } from "../Interfaces";

export const DealMockList: IDeal[] = [
  {
    dealId: 1,
    percentDiscount: 50,
  },
  {
    dealId: 2,
    percentDiscount: 25,
  },
  {
    dealId: 3,
    percentDiscount: 33,
  },
  {
    dealId: 4,
    percentDiscount: 40,
  },
  {
    dealId: 5,
    percentDiscount: 45,
  },
  {
    dealId: 6,
    percentDiscount: 90,
  },
  {
    dealId: 7,
    percentDiscount: 10,
  },
  {
    dealId: 8,
    percentDiscount: 15,
  },
  {
    dealId: 9,
    percentDiscount: 12,
  },
  {
    dealId: 10,
    percentDiscount: 13,
  },
];

export default DealMockList;
