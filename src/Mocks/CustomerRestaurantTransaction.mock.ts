import { ICustomerRestaurantTransaction } from "../Interfaces";

export const CustomerRestaurantTransactionMock: ICustomerRestaurantTransaction[] = [
  {
    transactionId: 1,
    discountCardNumber: 25,
    dealId: 17,
    date: "10/18/2003",
    chainLocationId: 44,
  },
  {
    transactionId: 2,
    discountCardNumber: 52,
    dealId: 22,
    date: "9/10/2003",
    chainLocationId: 22,
  },
  {
    transactionId: 3,
    discountCardNumber: 51,
    dealId: 55,
    date: "2/01/2004",
    chainLocationId: 12,
  },
  {
    transactionId: 4,
    discountCardNumber: 53,
    dealId: 44,
    date: "10/15/2003",
    chainLocationId: 55,
  },
  {
    transactionId: 5,
    discountCardNumber: 54,
    dealId: 14,
    date: "01/12/2003",
    chainLocationId: 99,
  },
  {
    transactionId: 6,
    discountCardNumber: 55,
    dealId: 135,
    date: "02/05/2003",
    chainLocationId: 11,
  },
  {
    transactionId: 7,
    discountCardNumber: 56,
    dealId: 33,
    date: "10/18/2003",
    chainLocationId: 13,
  },
  {
    transactionId: 8,
    discountCardNumber: 57,
    dealId: 20,
    date: "10/18/2003",
    chainLocationId: 11,
  },
  {
    transactionId: 9,
    discountCardNumber: 58,
    dealId: 12,
    date: "10/18/2003",
    chainLocationId: 11,
  },
  {
    transactionId: 10,
    discountCardNumber: 59,
    dealId: 15,
    date: "10/18/2003",
    chainLocationId: 12,
  },
];
